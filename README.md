ClassicMap
==========
<div><a href="http://click.linksynergy.com/fs-bin/click?id=SsjUadZH7vs&subid=&offerid=94348.1&type=10&tmpid=3910&RD_PARM1=http%3A%2F%2Fitunes.apple.com%2Fapp%2Fclassicmap%2Fid566173771%3Fmt%3D8"><img src="https://github.com/downloads/kishikawakatsumi/ClassicMap/appstore_badge.png" alt="Download on the App Store" width="270px" style="width: 270px;" /></a></div>

**Google Map is back to iOS 6.**  
A sample application to render Google Map tile images overlays in an Apple's map in iOS 6.
  
I borrowed many code from [iOS-MapLayerDemo](https://github.com/mtigas/iOS-MapLayerDemo). Thank you very much.  

------
<img src="https://github.com/downloads/kishikawakatsumi/ClassicMap/5.png" alt="ScreenShot1" width="225px" style="width: 225px;" />&nbsp;
<img src="https://github.com/downloads/kishikawakatsumi/ClassicMap/6.png" alt="ScreenShot2" width="225px" style="width: 225px;" />&nbsp;
<img src="https://github.com/downloads/kishikawakatsumi/ClassicMap/7.png" alt="ScreenShot3" width="225px" style="width: 225px;" />&nbsp;

## 3rd party libraries

**AFNetworking**  
[https://github.com/AFNetworking/AFNetworking](https://github.com/AFNetworking/AFNetworking)  
Distributed under the [MIT License][MIT].
 
[Apache]: http://www.apache.org/licenses/LICENSE-2.0
[MIT]: http://www.opensource.org/licenses/mit-license.php
[GPL]: http://www.gnu.org/licenses/gpl.html

## Contributors
**Artwork and Icon Design**  
[Takayuki Fukatsu](https://github.com/fladdict) a.k.a [fladdict](http://fladdict.net/blog/)

## License

ClassicMap is available under the MIT license. See the LICENSE file for more info.
